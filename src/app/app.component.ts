import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  clicker='Recipes';
  title = 'app';
  onClickReallyHandled(clicker){
    this.clicker=clicker;
  }
}
