import  {Directive, Renderer2, ElementRef, HostListener} from '@angular/core'

@Directive({
    selector: '[appDropDown]'
})
export class DropDownDirective{
    constructor(private ElementRef: ElementRef, private renderer: Renderer2){}
    clickToggle: Boolean=false;

    @HostListener('click') clickEvent(){
        this.clickToggle=!this.clickToggle;
        this.clickToggle? this.renderer.addClass(this.ElementRef.nativeElement, 'open'):this.renderer.removeClass(this.ElementRef.nativeElement, 'open')
    }
}