import { Component, OnInit } from '@angular/core';
import {Ingredient} from '../shared/ingredient.model';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {
  ingredients: Ingredient[]=[
    new Ingredient('Apples', 5),
    new Ingredient('Oranges', 10)
  ];
  constructor() { }
  ngOnInit() { }

  onIngredientAdded(serverData:{name: string, amount: string}){
    var position= this.isIngredientAlreadyThere(serverData.name);
    if(position==-1)
      this.ingredients.push(new Ingredient(serverData.name, parseFloat(serverData.amount)));
    else
      this.ingredients[position].amount+= parseFloat(serverData.amount);
  }
  onIngredientDeleted(serverData:{name: string, amount: string}){
    var position= this.isIngredientAlreadyThere(serverData.name);
    if(position!==-1){
      this.ingredients[position].amount-= parseFloat(serverData.amount);      
    
    if(this.ingredients[position].amount<=0 )
      this.ingredients.splice(position, 1);
    }
  }
  isIngredientAlreadyThere(name: string){
    for(var i=0;i< this.ingredients.length;i++)
      if(name.toLowerCase()===this.ingredients[i].name.toLowerCase())
        return i;
    return -1;
    
  }

}
