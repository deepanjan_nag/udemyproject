import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit {
  constructor(){}
  ngOnInit() {}
  @Output() added=new EventEmitter<{name: string, amount: string}>();
  @Output() deleted=new EventEmitter<{name: string, amount: string}>();

  onAdding(name, amount){
    this.added.emit({name: name, amount: amount});
  }
  onDeleting(name, amount){
    this.deleted.emit({name: name, amount: amount});
  }
}
