import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {Recipe} from '../recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  recipes: Recipe[] =[new Recipe('A Test recipe', 'This is simply a test', 'https://upload.wikimedia.org/wikipedia/commons/a/a1/Dry_Spiced_Chicken_Curry.jpg'),
  new Recipe('A 2nd Test recipe', 'This is another test', 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Boiled_Egg_Curry.jpg/1024px-Boiled_Egg_Curry.jpg')];
  constructor() { }
  ngOnInit() { }
  @Output() recipeReceivedAndSent=new EventEmitter<Recipe>();
  onRecipeClicked(rec: Recipe){
    this.recipeReceivedAndSent.emit(rec);
  }
}
