import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import {Recipe} from '../../recipe.model';

@Component({
  selector: 'app-recipe-item',
  templateUrl: './recipe-item.component.html',
  styleUrls: ['./recipe-item.component.css']
})
export class RecipeItemComponent implements OnInit {
  @Input() recipeElement:Recipe;
  @Output() recipeClicked=new EventEmitter<Recipe>();
  onRecipeClicked(recElement){
    this.recipeClicked.emit(recElement);
  }

  constructor() {}
  ngOnInit() {}
}
